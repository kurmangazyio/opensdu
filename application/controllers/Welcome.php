<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->register();
			return;
		}

		$this->load->model('blog');
		
		$about = include 'data/about.php';
		$schedules = include 'data/schedule.php';
		$faq = include 'data/faq.php';
		$speakers = include 'data/speakers.php';
		$phrase = include 'data/phrase.php';
		
		$data = [
			'about' => $about,
			'schedule' => $schedules,
			'speakers' => $speakers,
			'faq' => $faq,

			'news' => $this->blog->all(),
			'teams' => [],
			'system' => [
				// main page
				'title' => 'Welcome to SDU Open',
				'small_description' => 'Hellow, mellow',

				//header
				'icon' => 'icon.png',
				'logo' => 'logo.png',

				//website info for SEO
				'author' => 'Suleyman Demirel University',
				'description' => '',
				'keywords' => 'SDU, open, sdu open, hackaton, contest, programming, kaskelen',
				'address' => 'Almaty region, Kaskelen city, Abylai khan street, 1/1',

				//contacts
				'email' => '@sdu.edu.kz',
				'phone' => 'phone',
				'footer_title' => 'Open SDU, 2021. Suleyman Demirel University',

				'background' => base_url().'assets/img/back.png'
			],
			'phrase' => $phrase
		];

		$this->load->view('website', $data);
	}


	public function register(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$data = $this->input->post('data');
			$count = $this->input->post('count');
			$team = $data['team'];
			$added = false;

			$this->load->model('team');
			$check_if_team_exists 		= $this->team->check($team);
			$check_if_students_exist 	= $this->team->check_students($data);

			if(!$check_if_team_exists && !$check_if_students_exist)
				$added 	= $this->team->register($data, $count);

			
			header('Content-Type: application/json');
    		echo json_encode([
    			'team_exists' => $check_if_team_exists,
    			'students_exist' => $check_if_students_exist,
    			'registered' => $added
    		]);
		}
		
	}
}
