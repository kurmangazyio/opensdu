<?php

if (isset($_GET['admin']) && isset($_GET['pass'])) {
	# code...
	if ($_GET['admin'] == 'admin' && $_GET['pass'] == 'max') {
		# code...
		$servername = "localhost";
		$username = "root";
		$password = "";
		$db = 'opensdu';

		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $db);
		$sql = "select teams.teamid as teamidid, teams.name as teamname, students.* from students inner JOIN teams on teams.id = students.teamid;";
		$query = mysqli_query($conn, $sql);
	}else{
		return 'no auth';
	}
}
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/4.1/assets/img/favicons/favicon.ico">

    <title>Admin</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/grid/">
    <link href="https://getbootstrap.com/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="grid.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
  </head>

  <body>
    <div class="container">
      	<h1>Admin all participant teams and students</h1>
      	<p class="lead">Basic grid layouts to get you familiar with building within the Bootstrap grid system.</p>

      	<div class="row">
        	<div class="col-sm-12">
        		<table id="table_id" class="display">
				    <thead>
				        <tr>
				            <th>Team ID</th>
				            <th>Team NAME</th>
				            <th>Student Fullname</th>
				            <th>Email</th>
				            <th>School</th>
				            <th>Phone</th>
				            <th>Message</th>
				            <th>Registed time</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php while($data = mysqli_fetch_array($query)){ ?>
						<tr>
						    <td><?php echo $data["teamidid"]; ?></td>
						    <td><?php echo $data["teamname"]; ?></td>
						    <td><?php echo $data["fullname"]; ?></td>
						    <td><?php echo $data["email"]; ?></td>
						    <td><?php echo $data["school"]; ?></td>
						    <td><?php echo $data["phone"]; ?></td>
						    <td><?php echo $data["message"]; ?></td>
						    <td><?php echo $data["created_at"]; ?></td>
						</tr>
						<?php } ?>
				    </tbody>
				</table>
        	</div>
      	</div>
    </div>
    <script type="text/javascript">
    	$(document).ready( function () {
		    $('#table_id').DataTable({
		        dom: 'Bfrtip',
		        buttons: [
		            'copy', 'csv', 'excel', 'pdf', 'print'
		        ]
		    });
		} );
    </script>
  </body>
</html>