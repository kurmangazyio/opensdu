<?php

return [
	// about
	'title' => 'About the !',
	'small_description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium',
	'description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium',
	'faq_title' => 'Frequently Asked Questions',
	// schedule
	'schedule_title' => 'Event schedule',
	'schedule_small_description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium',
	// speakers
	'speakers_title' => 'Event Speakers',
	'speakers_small_description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium',
	'speakers' => 'Speakers',
	// latest press
	'press_title' => 'Latest updates',
	'press_small_description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium',
	'press_ps_text' => 'Please, subscribe into our telegram channel!',
	// register
	'register_title' => 'Register your team',
	'register_small_description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium',
	'register_ps_text' => 'Please, subscribe into our telegram channel!',

	//register warning messages
	'register_warning_message' => 'Please, subscribe into our telegram channel!',
];
