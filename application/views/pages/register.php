<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content__inner">
	<div class="section full-height over-y-scroll" id="boxscroll-1">
		<div class="section" id="boxscroll-in-1">

			<div class="section padding-top-big">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="content__title"><?php echo $about['register_title']; ?></h2>
						</div>
						<div class="col-lg-4 mt-3">
							<h3 class="content__subtitle"><?php echo $about['register_small_description']; ?></h3>
						</div>
						<div class="col-md-12 mt-4 text-center h6 text-warning">
							<p class="mt-4"><?php echo $about['description']; ?></p>
						</div>
					</div>
				</div>
			</div>	

			<div class="section padding-top padding-bottom-big">	
				<form name="ajax-form" id="ajax-form">
					<div class="container">
						<div class="row">		
							<div class="offset-md-3 col-md-9">
								<h4>Team information</h4>
								<div class="title-page-line mt-4 mb-3"></div>
							</div>	
							
							<div class="col-lg-3">
								<h6 class="mt-3">Team</h6>
							</div>

							<div class="col-lg-9 mt-4 mt-lg-0">
								<div class="row">
									<div class="col-lg-12">
										<label for="name"> 
											<span class="error" id="err-name">please enter team name</span>
										</label>
										<input name="team" id="team" type="text" placeholder="Your Team Name: *" required=""/>
									</div>
								</div>
							</div>

							<div class="col-lg-3"></div>
							<div class="col-lg-9 h6 text-warning">
								<p class="mt-4"><?php echo $about['register_warning_message']; ?></p>
							</div>	

							<div class="col-lg-3"></div>
							<div class="col-lg-9 mt-4">
								<div id="button-con">
									<button id="addstudent" class="send_message" type="button">
										<span>Add student to a team</span>
									</button>
									<a href="javascript:void(0)" id="removelaststudent" style="display: none;" class="pl-3 text-danger h6"> Remove last student </a>
								</div>					
							</div>	
							 
							<div class="col-lg-3"></div>
							<div class="col-lg-9 mt-4">
								<div class="row" id="studentslist">
								</div>				
							</div>

							<div class="col-lg-3"></div>
							<div class="col-lg-9 mt-4">
								<div id="button-con">
									<button class="send_message" type="submit">
										<span>submit</span>
									</button>
								</div>					
							</div>	
							<div class="col-lg-3"></div>
							<div class="col-lg-9">
								<div class="error" id="err-state">
									<p class="text-error p-3 h6 text-danger" id="errormessage">Successfully sent!!</p>
								</div>
								<div id="ajaxsuccess">
									<p class="text-success p-3 h6">Your team and students are successfully registered!!</p>
								</div>
							</div>	

						</div>
					</div>
				</form>
			</div>	
							
			<?php $this->load->view('pages/footer', $system ); ?>
		</div>
	</div>
</div>