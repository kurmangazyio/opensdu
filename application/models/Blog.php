<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Model {

	public function all(){
		$blogs 	= [];
		$result = $this->db->get('blog')->result();

		foreach ($result as $res) {
			# code...
			$blog['title'] = $res->title;
			$blog['content'] = $res->content;
			$blog['datestamp'] = $res->datestamp;
			$blog['extra'] = $res->extra;
			$blog['created_at'] = $res->created_at;

			array_push($blogs, $blog);
		}
        return $blogs;
	}
}