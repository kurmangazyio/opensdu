--------------------- About --------------------------------

[
	'title' => 'About SDU Open!',
	'small_description' => 'HSPC-style programming contest organized by Suleyman Demirel University',
	'description' => 'The HSPC-style programming contest hosted by the Faculty of Engineering and Natural Sciences at Suleyman Demirel University, brings talented students from high schools and colleges to participate in an organized competition. Students compete in teams to demonstrate their programming skills and math problem solving abilities by attempting to solve 10 programming problems within a four hour period.'
]


--------------------- Schedule -----------------------------

[
	'title' => 'Opening Registration for contest',
	'description' => 'Students start to join for battle',
	'date' => 'Wednesday, 14 april 2021',
	'time' => '10:42 AM'
],
[
	'title' => 'Closing Registration',
	'description' => 'The battle doors are closes',
	'date' => 'Wednesday, 28 april 2021',
	'time' => '11:53 PM'
],
[
	'title' => 'Sending invitation for contest',
	'description' => 'Students receives battle tickets',
	'date' => 'Sunday, 02 may 2021',
	'time' => '08:36 AM'
],
[
	'title' => 'Starting of SDU Open',
	'description' => 'Battle time!',
	'date' => 'Sunday, 02 may 2021',
	'time' => '11:01 AM - 03:10 PM'
],
[
	'title' => 'Unfreeze Leaderboard',
	'description' => 'Who is winner?',
	'date' => 'Sunday, 02 may 2021',
	'time' => '03:12 PM'
]

---------------------------------- faq ------------------------------

[
	'question' => 'Who can participate?',
	'answer' => '10-11 graders of schools and college students'
],
[
	'question' => 'How many people should be in a team?',
	'answer' => 'Teams should be formed out of a maximum of 3 people.'
],
[
	'question' => 'How long is the competition?',
	'answer' => 'The contest will consist of 10 problems to be solved over a 4 hour period. The set of problems will span various levels of difficulty.'
],
[
	'question' => 'How will you submit your solutions?',
	'answer' => 'We will be using CodeForces as the problem submission system. You can take a quick tutorial on how to use CodeForces.'
],
[
	'question' => 'Where can I find some sample questions?',
	'answer' => 'Here should be same sample questions.'
]