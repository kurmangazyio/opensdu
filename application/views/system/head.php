<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
		<meta charset="utf-8">
		<title><?php echo $title; ?></title>
		<meta name="description"  content="<?php echo $description; ?>" />
		<meta name="author" content="<?php echo $author; ?>">
		<meta name="keywords"  content="<?php echo $keywords; ?>" />

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="theme-color" content="#212121"/>
		<meta name="msapplication-navbutton-color" content="#212121"/>
		<meta name="apple-mobile-web-app-status-bar-style" content="#212121"/>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"/>
		<link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.webui-popover.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.min.css"/>

		<link rel="icon" type="image/png" href="<?php echo base_url().$icon; ?>">
		<link rel="apple-touch-icon" href="<?php echo base_url().$icon; ?>">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url().$icon; ?>">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url().$icon; ?>">

		<script>document.documentElement.className="js";var supportsCssVars=function(){var e,t=document.createElement("style");return t.innerHTML="root: { --tmp-var: bold; }",document.head.appendChild(t),e=!!(window.CSS&&window.CSS.supports&&window.CSS.supports("font-weight","var(--tmp-var)")),t.parentNode.removeChild(t),e};supportsCssVars()||alert("");
		</script> 