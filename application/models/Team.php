<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Model {

	public function check($team){
		return $this->checker($team, 'teams', 'name');
	}

	public function check_students($data)
	{
		# code...
		$status = 0;

		for ($i=0; $i < 3; $i++) { 
			# code...
			if (isset($data['emailst'.$i])) {
				# code...
				if($this->checker($data['emailst'.$i], 'students', 'email'))
					$status++;
			}
		}

		return $status > 0 ? true : false;
	}

	public function checker($data, $table, $where)
	{
		# code...
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where, $data);

		return $this->db->count_all_results() > 0 ? true : false;
	}

	public function register($data, $count)
	{
		# code...
		$team = $this->registerTeam($data['team']);

		for ($i = 0; $i < $count; $i++) { 
			# code...
			$rrr = array(
			    'teamid' => $team,
			    'fullname' => $data['fullnamest'.$i],
			    'email' => $data['emailst'.$i],
			    'school' => $data['schoolst'.$i],
			    'phone' => $data['phonest'.$i],
			    'message' => $data['messagest'.$i]
			);

			$this->db->insert('students', $rrr);
		}

		return true;
	}

	public function registerTeam($team)
	{
		# code...
		$id = $this->generateRandomString();

		$this->db->insert('teams', ['teamid' => $id, 'name' => $team]);

		$this->db->select('*');
		$this->db->from('teams');
		$this->db->where('teamid', $id);

		$query = $this->db->get();
		$ret = $query->row();

		return $ret->id;
	}

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}