var students = 0;
var max = 3;

		(function($) { "use strict";
	
	$(document).ready(function() {

		
		$("#ajax-form").on("submit", function(e) {
			// body...
			e.preventDefault();

			$('#ajaxsuccess').css('display', 'nono');
			$('#err-state').css('display', 'none');

			var data = $(this).serializeArray().reduce(function(obj, item) { obj[item.name] = item.value; return obj; }, {});
			var url = window.location.href;
			$.ajax({
			  type: "POST",
			  url: url,
			  data: {
			  	data: data,
			  	count: students
			  },
			  success: function (res){

			  	if (res.registered) {
			  		$('#ajaxsuccess').css('display', 'inline');
			  		const myNode = document.getElementById("studentslist");
  					myNode.innerHTML = '';
  					students = 0;
  					$('#team').value('');

  					if (students == 0) {
						$('#removelaststudent').css('display', 'none');
					}
			  	}

			  	if (res.students_exist || res.team_exists) {
			  		$('#err-state').css('display', 'inline');

			  		if (res.team_exists) {
			  			$('#errormessage').html('This name is taken, try to rename your team!');
			  		}

			  		if (res.students_exist) {
			  			$('#errormessage').html('Student with the email exists, it means one of your students whom you are try to register is exists in our database.');
			  		}
			  	}
			  }
			});

			//console.log(data);
		});

		$('#addstudent').on('click', function(e) {
			// body...
			if (students != max) {
				$('#studentslist').append(
					'<div class="col-lg-12 mt-4 mb-4 student' + students +'">' +
						'<h5>Student ' + (students + 1) +'</h5>' +
					'</div>' +
					
					'<div class="col-lg-6 student' + students +'"">' + 
						'<label for="fullname">' +
							'<span class="error" id="err-fullname">please enter fullname</span>' +
						'</label>' +
						'<input name="fullnamest' + students +'" type="text" placeholder="Full Name: *" required/>' +
					'</div>' +

					'<div class="col-lg-6 mt-4 mt-lg-0 student' + students +'"">' +
						'<label for="email' + students + '">' +
							'<span class="error" id="err-email">please enter e-mail</span>' +
						'</label>' +
						'<input name="emailst' + students +'" id="email' + students + '" type="email" placeholder="E-Mail: *" required/>' +
					'</div>' +
					'<div class="col-lg-6 pt-4 mt-lg-0 student' + students +'"">' +
						'<label for="school' + students + '">' +
							'<span class="error" id="err-school">please enter school</span>' +
						'</label>' +
						'<input name="schoolst' + students + '" id="school' + students + '" type="text" placeholder="School name: *" required/>' +
					'</div>' +
					'<div class="col-lg-6 pt-4 mt-lg-0 student' + students +'"">' +
						'<label for="phone' + students + '">'+
							'<span class="error" id="err-phone">please enter phone</span>'+
						'</label>' +
						'<input name="phonest' + students + '" id="phone' + students + '" type="text" placeholder="Phone number: "/>' +
					'</div>' +
					'<div class="col-lg-12 mt-4 student' + students +'"">' +
						'<label for="message' + students + '"></label>' +
						'<textarea rows="2" name="messagest' + students + '" id="message' + students + '" placeholder="Tell us about your self"></textarea>' +
					'</div>'
				);
			}

			$('#removelaststudent').css('display', 'inline');
			students = students + 1;
		});

		$('#removelaststudent').on('click', function () {
			// body...
			var classname = students - 1;
			$('.student' + classname).remove();

			students = students - 1;
			if (students == 0) {
				$('#removelaststudent').css('display', 'none');
			}
		})
	});	
	
})(jQuery); 