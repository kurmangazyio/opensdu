<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php $this->load->view('system/head', $system); ?>	
	</head>
	<body class="aoide">
		<div class="loader">
			<div class="loader__figure"></div>
		</div>
		<main style="background-image: url(<?php echo base_url().$system['background']; ?>);">
			<a href="<?php echo base_url(); ?>">
				<div class="logo">
					<img src="<?php echo base_url().$system['logo']; ?>" alt="">
				</div>
			</a>

			<a href="#ticket-wrap" class="ticket-link">
				<div class="ticket">
					<p><?php echo $phrase['top_register']; ?></p>
				</div>
			</a>

			<div id="ticket-wrap">
				<p style="text-align: center;"><?php echo $phrase['top_register_description']; ?></p>
				<a href="javascript:void()" onclick="registerteam()" style="margin-top: 20px;"><?php echo $phrase['top_register']; ?></a>
			</div>


			<?php $this->load->view('content/content', $system); ?>	


			<div class="content content--reveal">
				<?php $this->load->view('pages/about', ['about'=>$about,'faqs'=>$faq,'system'=>$system]);?>
				<?php $this->load->view('pages/schedule', ['about'=>$about,'system'=>$system,'schedule'=>$schedule,'speakers'=>$speakers]);?>
				<?php $this->load->view('pages/press', ['about'=>$about,'news'=>$news,'system'=>$system]);?>
				<?php $this->load->view('pages/register', ['about'=>$about, 'system'=>$system]);?>
				<button class="content__close" id="content__close"></button>
			</div>
			
			<div class="location-fixed">
				<p><?php echo $system['address']; ?> </p>
			</div>
		</main>
		<?php $this->load->view('system/footer', $system); ?>
	</body>
</html>