<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content__inner">
	<div class="section full-height over-y-scroll" id="boxscroll-1">
		<div class="section" id="boxscroll-in-1">
			<!-- Page start -->
			<div class="section padding-top-big padding-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="content__title"><?php echo $about['press_title']; ?></h2>
						</div>
						<div class="col-lg-4 mt-3">
							<h3 class="content__subtitle"><?php echo $about['press_small_description']; ?></h3>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="section">
				<div class="container">
					<div class="row">	
						<?php foreach($news as $new) { ?>
						<div class="col-lg-3 mb-4">
							<h6><?php echo $new['datestamp']; ?></h6>
						</div>
						<div class="col-lg-9 mt-4 mt-lg-0 mb-4">
							<div class="blog-wrap pb-4">
								<h6 class="px-4 mt-4"><?php echo $new['title']; ?></h6>
								<p class="px-4 mb-3 mb-1">
									<small>by Admin</small>
								</p>
								<p class="px-4"><?php echo $new['content']; ?></p>
							</div>	
						</div>	
						<?php } ?>
					</div>
				</div>
			</div>	
			<div class="section padding-bottom-big">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="mt-4 h6" style="text-align: center;"><?php echo $about['press_ps_text']; ?></p>
						</div>
					</div>
				</div>
			</div>	
			<style type="text/css">
				.red{
					color: #e74c3c !important;
				}
			</style>			
			<?php $this->load->view('pages/footer', $system ); ?>
		</div>
	</div>
</div>