<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="section padding-top padding-bottom-big background-dark footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="<?php echo base_url().$logo; ?>" alt="">
				<h6 class="mt-5 mb-3">Get in touch:</h6>
				<p><?php echo $email; ?></p>
				<p><?php echo $phone; ?></p>
			</div>
			<div class="col-md-12 mt-5 mb-5">
				<div class="title-page-line"></div>
			</div>
			<div class="col-md-12">
				<p><small><?php echo $footer_title; ?></small></p>
			</div>
		</div>
	</div>
</div>