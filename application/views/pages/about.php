<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content__inner">
	<div class="section full-height over-y-scroll" id="boxscroll-1">
		<div class="section" id="boxscroll-in-1">
			<!-- Page start -->
			<div class="section padding-top-big padding-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="content__title"><?php echo $about['title']; ?></h2>
						</div>
						<div class="col-lg-4 mt-3">
							<h3 class="content__subtitle" style="font-size: 16px;"><?php echo $about['small_description']; ?></h3>
						</div>
						<div class="col-md-12">
							<p class="mt-4 h5"><?php echo $about['description']; ?></p>
						</div>
					</div>
				</div>
			</div>
			
			<div class="section padding-bottom-big">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h4><?php echo $about['faq_title']; ?></h4>
							<div class="title-page-line mt-4 mb-5"></div>
						</div>		
						<?php foreach($faqs as $faq) { ?>
						<div class="col-lg-4 mb-4">
							<h6 class="pt-4"><?php echo $faq['question']; ?></h6>
						</div>
						<div class="col-lg-8 mt-4 mt-lg-0 mb-4">
							<div class="pass-wrap">
								<p class="pl-0"><?php echo $faq['answer']; ?></p>
							</div>	
						</div>	
						<?php } ?>
					</div>
				</div>
			</div>					
			<?php $this->load->view('pages/footer', $system ); ?>
		</div>
	</div>
</div>