<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content__inner">
	<div class="section full-height over-y-scroll" id="boxscroll-1">
		<div class="section" id="boxscroll-in-1">
			<!-- Page start -->
			<div class="section padding-top-big">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="content__title"><?php echo $about['schedule_title']; ?></h2>
						</div>
						<div class="col-lg-4 mt-3">
							<h3 class="content__subtitle"><?php echo $about['schedule_small_description']; ?></h3>
						</div>
					</div>
				</div>
			</div>	
			<div class="section padding-top-bottom">
				<div class="container">
					<div class="row">
						<?php foreach($schedule as $sched) { ?>
						<div class="offset-md-3 col-md-9">
							<h4><?php echo $sched['title']; ?></h4>
							<p class="mt-2"><?php echo $sched['date']; ?></p>
							<div class="title-page-line mt-4 mb-3"></div>
						</div>						
						<div class="col-md-3">
							<div class="schedule-time mb-2">
								<p><?php echo $sched['time']; ?></p>
							</div>
						</div>
						<div class="col-md-9 mt-md-0">
							<p style="font-size: 18px;"><?php echo $sched['description']; ?></p>
						</div>		
						<?php } ?>				
					</div>
				</div>
			</div>	
			<div class="section padding-top-big">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h3 class="content__title"><?php echo $about['speakers_title']; ?></h3>
						</div>
						<div class="col-lg-4 mt-3">
							<h3 class="content__subtitle"><?php echo $about['speakers_small_description']; ?></h3>
						</div>
					</div>
				</div>
			</div>		

			<div class="section padding-top-bottom">
				<div class="container">
					<div class="row">
						<div class="offset-md-3 col-md-9">
							<h4><?php echo $about['speakers']; ?></h4>
							<div class="title-page-line mt-4 mb-5"></div>
						</div>
						<?php foreach($speakers as $speaker) { ?>
						<div class="col-md-3">
							<div class="speaker-image">
								<h6><?php echo $speaker['position']; ?></h6>
								<img src="<?php echo $speaker['image']; ?>" alt="<?php echo $speaker['fullname']; ?>" title="<?php echo $speaker['fullname']; ?>">
							</div>
						</div>
						<div class="col-md-9 mt-4 mt-md-0">
							<h5><?php echo $speaker['fullname']; ?></h5>
							<p><em><?php echo $speaker['description']; ?></em></p>
							<p class="mt-4"><?php echo $speaker['info']; ?></p>
						</div>
						<?php } ?>	
					</div>
				</div>
			</div>

			<?php $this->load->view('pages/footer', $system ); ?>
		</div>
	</div>
</div>